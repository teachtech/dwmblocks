//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	        /*Command*/		/*Update Interval*/	/*Update Signal*/
	{"",	            "sts-gupgrades",	0,	                8},
	{"",            	"sts-email",	    0,                 10},
	{"",            	"sts-news",	        0,              	9},
	{"",            	"sts-torrent",	    1,              	7},
	{"",            	"sts-music_mpd",	0,              	6},
	{"",            	"sts-volume",	    0,              	5},
	{"",	            "sts-memcpu",	    1,              	4},
	{"",	            "sts-battery",	    5,              	3},
	{"",	            "sts-netw",		    5,              	2},
	{"",	            "sts-datetime",	    1,              	1},
    {"  "},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = "  ";
static unsigned int delimLen = 5;

// Have dwmblocks automatically recompile and run when you edit this file in
// vim with the following line in your vimrc/init.vim:

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
